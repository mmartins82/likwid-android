LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS := $(GLOBAL_CFLAGS) -DMAX_NUM_THREADS=263 -DMAX_NUM_NODES=4 -DVERSION=3 -DRELEASE=1 -DACCESSMODE=0 -DHAS_MEMPOLICY=0 -std=c99

LOCAL_C_INCLUDES := bionic
LOCAL_C_INCLUDES += $(LOCAL_PATH)/includes

LOCAL_SRC_FILES := \
	strUtil.c \
	timer.c \
	power.c \
	thermal.c \
	cpuid.c \
	accessClient.c \
	numa.c \
	affinity.c \
	tree.c \
	bstrlib.c \
	pci.c \
	bitUtil.c \
	timer.c \
	msr.c

LOCAL_MODULE := meter

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_CFLAGS := $(GLOBAL_CFLAGS) -DMAX_NUM_THREADS=263 -DMAX_NUM_NODES=4 -DVERSION=3 -DRELEASE=1 -DACCESSMODE=0 -std=c99
LOCAL_C_INCLUDES := bionic
LOCAL_C_INCLUDES += $(LOCAL_PATH)/includes

LOCAL_SRC_FILES := \
	applications/likwid-powermeter.c

LOCAL_MODULE := powermeter

LOCAL_STATIC_LIBRARIES := libmeter

include $(BUILD_EXECUTABLE)
